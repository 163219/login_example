var express = require("express");
var app = express();

var check = require("connect-ensure-login");

var session = require("express-session");
var bodyParser = require("body-parser");

var count = 0;

var passport = require("passport");
var PassportLocal  = require("passport-local").Strategy;
const local = new PassportLocal(
    {
        usernameField: "email",
        passwordField: "password"
    }, 
    function(username, password, done) {
        console.info("authenticating: %s, %s", username, password);
        //TODO: access to database to check for validity
        //select password from user_table where username like ?
        if (username == password)
            return (done(null, 
                { 
                    loginId: username,
                    loginTime: new Date()
                }));
        return (done(null, false));
    }
);

passport.serializeUser(function(loginInfo, done) {
    console.info("IN serializeUser: %s", JSON.stringify(loginInfo));
    done(null, loginInfo.loginId);
});
passport.deserializeUser(function(uid, done) {
    console.info("IN deserializeUser: %s", uid);
    //Now we use uid to pull user details from database and enrich
    //user object req.user
    done(null, {
        username: uid,
        loginTime: new Date(),
        counter: count++
    });
})

passport.use(local);

//Process request
app.use(session({
    secret: "pukcats",
    resave: false,
    saveUninitialized: false
}));
app.use(bodyParser.urlencoded({extended: false}));

app.use(passport.initialize());
app.use(passport.session());

app.use("/protected/*", 
    check.ensureLoggedIn("/login.html"), 
    function(req, res, next) {
        console.info("User data: %s", JSON.stringify(req.user));
        next();
    });

// content: application/x-www-form-urlencoded
app.post("/authenticate", 
    passport.authenticate("local", {
        successReturnToOrRedirect: "/protected/home.html",
        failureRedirect: "/incorrect_login.html"
    }));

app.get("/logout", function(req, res) {
    req.logout();
    res.redirect("/login.html");
})

// content: application/x-www-form-urlencoded
app.post("/protected/add-to-cart", function(req, res) {
    if (!req.session.cart)
        req.session.cart = [];
    req.session.cart.push(req.body.item);
        
});


app.use(express.static(__dirname + "/public"));

app.listen(3000, function() {
    console.info("Application started on port 3000");
})




















